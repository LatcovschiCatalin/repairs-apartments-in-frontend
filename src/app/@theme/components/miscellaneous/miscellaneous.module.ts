import { NgModule } from '@angular/core';
import {NbButtonModule, NbCardModule, NbLayoutModule} from '@nebular/theme';

import { MiscellaneousRoutingModule } from './miscellaneous-routing.module';
import { MiscellaneousComponent } from './miscellaneous.component';
import { NotFoundComponent } from './not-found/not-found.component';
import {ThemeModule} from "../../theme.module";
import {SharedModule} from "../../../landing/shared/shared.module";

@NgModule({
    imports: [
        ThemeModule,
        NbCardModule,
        NbButtonModule,
        NbLayoutModule,
        MiscellaneousRoutingModule,
        SharedModule,
    ],
  declarations: [
    MiscellaneousComponent,
    NotFoundComponent,
  ],
})
export class MiscellaneousModule { }
