import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private http: HttpClient) {
  }

  get(options, page): Observable<any> {
    return this.http.get(environment.api_url + `/api/messages?page=${page}`, options);
  }

  getById(options, page, id): Observable<any> {
    return this.http.get(environment.api_url + `/api/messages/${id}?page=${page}`, options);
  }
}
