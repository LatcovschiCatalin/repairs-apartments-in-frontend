import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SeeMessagesComponent} from './see-messages/see-messages.component';
import {MessagesService} from "./messages.service";
import {MessagesRoutingModule} from "./messages-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [SeeMessagesComponent],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    NbCardModule,
    ReactiveFormsModule,
    NbButtonModule,
    SharedModule,
  ],
  providers: [
    MessagesService
  ]
})
export class MessagesModule {
}
