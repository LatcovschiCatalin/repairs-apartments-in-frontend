import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PhotoAlbumsService {

  constructor(private http: HttpClient) {
  }

  get(page): Observable<any> {
    return this.http.get(environment.api_url + `/api/photo-albums?page=${page}`);
  }

  getById(id, page): Observable<any> {
    return this.http.get(environment.api_url + `/api/photo-albums/${id}?page=${page}`);
  }

  createAlbum(album, page, options): Observable<any> {
    return this.http.post(environment.api_url + `/api/photo-albums?page=${page}`, album, options);
  }

  updateAlbum(album, id, page, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/photo-albums/${id}?page=${page}`, album, options);
  }}
