import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../../../environments/environment";
import {PhotoAlbumsService} from "../photo-albums.service";

@Component({
  selector: 'app-create-album',
  templateUrl: './create-album.component.html',
  styleUrls: ['./create-album.component.scss']
})
export class CreateAlbumComponent implements OnInit {
  totalDocs;
  totalPages = [];
  page;
  image_src = [];
  file;
  imageUpload;
  editImage = [];
  isEditPage = false;
  columns: {}
  response;
  data;
  id;
  albums = [
    {
      title: '',
      description: '',
      mainProductImage: {}
    }
  ];
  object = {
    title: '',
    mainProductImage: {},
    albums: [],
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: PhotoAlbumsService) {

    this.file = []

    route.queryParams.subscribe(p => {
      this.id = p.Id;

      if (p.page) {
        this.page = Number(p.page);
      } else {
        this.page = 1;
        this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page, Id: this.id}});
      }

      this.editImage = []
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById(this.id, this.page).subscribe(data => {
          this.object = data;
          this.albums = data.albums;
          for (let i = 0; i < data.albums.length; i++) {
            if (data.albums[i].mainProductImage && data.albums[i].mainProductImage.path) {
              this.image_src[i] = data.albums[i].mainProductImage;
              this.editImage[i] = [{
                url: `${environment.api_url}/api/` + data.albums[i].mainProductImage.path.replaceAll('\\', '/'),
                fileName: data.albums[i].mainProductImage.description
              }];

            }
          }


        });
      }
    });
  }


  ngOnInit(): void {
    this.imageUpload = `${environment.api_url}/api/upload/photo-album/image`;

    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
      this.totalDocs = data.totalDocs;
      for (let i = 0; i < data.totalPages; i++) {
        this.totalPages[i] = i + 1;
        this.totalDocs = data.totalDocs;
      }
    })

    this.columns = {
      id: {
        title: 'ID'
      },
      title: {
        title: 'TITLE'
      },
    }

  }

  nextPage() {
    this.page += 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  getPage(page) {
    this.page = Number(page);
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  prevPage() {
    this.page -= 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  addPhoto() {
    this.albums.push({
      title: '',
      description: '',
      mainProductImage: {}
    })
  }

  deletePhoto(index) {
    this.albums.splice(index, 1);
    this.image_src = [];
    this.editImage = [];
  }

  open(dialog: TemplateRef<any>) {
    this.editImage = [];
    this.image_src = [];
    this.albums = [
      {
        title: '',
        description: '',
        mainProductImage: {}
      }
    ];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      title: '',
      mainProductImage: {},
      albums: [],
    };
  }


  onSubmit() {

    this.object.albums = this.albums;

    if (this.isEditPage) {
      this.service.updateAlbum(JSON.stringify(this.object), this.id, this.page, {headers: this.accessToken.headers}).subscribe(res => {
          this.service.get(this.page).subscribe(data => {
            this.response = data.docs;
            for (let i = 0; i < data.totalPages; i++) {
              this.totalPages[i] = i + 1;
              this.totalDocs = data.totalDocs;
            }
          });
          this.service.getById(this.id, this.page).subscribe(data => {
            this.albums = data.albums;
            // @ts-ignore
            for (let i = 0; i < data.albums.length; i++) {
              this.editImage[i] = [{
                url: `${environment.api_url}/api/` + data.albums[i].mainProductImage.path.replaceAll('\\', '/'),
                fileName: data.albums[i].mainProductImage.description
              }];

            }
          });

        }
      );
    } else {
      this.service.createAlbum(JSON.stringify(this.object), this.page, {headers: this.accessToken.headers}).subscribe(res => {
        this.service.get(this.page).subscribe(data => {
          this.response = data.docs;
          this.totalDocs = data.totalDocs;
        });
      });
    }
  }

  uploadFinished(event, i) {

    if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body) {
      this.albums[i].mainProductImage = {
        path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
        mimetype: event.serverResponse.response.body.mimetype,
        description: event.serverResponse.response.body.filename
      }
    }
  }

  removeImage(i) {
    this.albums[i].mainProductImage = {};
    this.editImage = []
  }

}
