import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateAlbumComponent} from "./create-album/create-album.component";

const routes: Routes = [
  {
    path: '',
    component: CreateAlbumComponent,
    data: {name: 'PhotoAlbums'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoAlbumsRoutingModule {
}

export const routedComponents = [
  CreateAlbumComponent
];
