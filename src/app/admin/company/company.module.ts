import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateCompanyComponent} from './create-company/create-company.component';
import {CompanyService} from "./company.service";
import {UsersRoutingModule} from "./company-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {ImageUploadModule} from "angular2-image-upload";


@NgModule({
  declarations: [CreateCompanyComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NbCardModule,
    NbButtonModule,
    SharedModule,
    FormsModule,
    ImageUploadModule.forRoot(),
  ],
  providers: [CompanyService]
})
export class CompanyModule {
}
