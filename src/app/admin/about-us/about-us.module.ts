import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateAboutUsComponent} from './create-about-us/create-about-us.component';
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {AboutUsRoutingModule} from "./about-us-routing.module";
import {AboutUsService} from "./about-us.service";


@NgModule({
  declarations: [CreateAboutUsComponent],
  imports: [
    CommonModule,
    AboutUsRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
  ],
  providers: [
    AboutUsService
  ],
})
export class AboutUsModule {
}
