import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateAboutUsComponent} from "./create-about-us/create-about-us.component";

const routes: Routes = [
  {
    path: '',
    component: CreateAboutUsComponent,
    data: {name: 'AboutUs'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsRoutingModule {
}

export const routedComponents = [
  CreateAboutUsComponent
];
