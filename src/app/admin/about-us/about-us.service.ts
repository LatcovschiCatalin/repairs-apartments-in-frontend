import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AboutUsService {
  constructor(private http: HttpClient) {
  }

  get(): Observable<any> {
    return this.http.get(environment.api_url + `/api/about-us`);
  }

  getById(id): Observable<any> {
    return this.http.get(environment.api_url + `/api/about-us/${id}`);
  }

  createAboutUs(about_us, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/about-us?limit=9999999999999999999', about_us, options);
  }

  updateAboutUs(id, about_us, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/about-us/${id}`, about_us, options);
  }
}
