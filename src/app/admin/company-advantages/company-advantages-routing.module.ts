import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateCompanyAdvantagesComponent} from "./create-company-advantages/create-company-advantages.component";

const routes: Routes = [
  {
    path: '',
    component: CreateCompanyAdvantagesComponent,
    data: {name: 'CompanyAdvantages'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyAdvantagesRoutingModule {
}

export const routedComponents = [
  CreateCompanyAdvantagesComponent
];
