import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersRoutingModule} from "./users-routing.module";
import {UserService} from "./users.service";
import {UserComponent} from "./user/user.component";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NbCardModule,
    ReactiveFormsModule,
    NbButtonModule,
    SharedModule,
  ],
  providers: [UserService]
})
export class UsersModule {
}
