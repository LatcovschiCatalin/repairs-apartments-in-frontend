import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService, NbToastrService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {UserService} from "../users.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  totalDocs;
  totalPages = [];
  page;
  registerForm: FormGroup;
  submitted = false;
  response;
  columns: {};

  constructor(private formBuilder: FormBuilder,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private service: UserService,
              private route: ActivatedRoute,
              private router: Router) {

    route.queryParams.subscribe(p => {
      if (p.page) {
        this.page = Number(p.page);
      } else {
        this.page = 1;
        this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
      }

    })
  }


  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {});
  }


  nextPage() {
    this.page += 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  getPage(page) {
    this.page = Number(page);
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  prevPage() {
    this.page -= 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  ngOnInit() {


    this.columns = {
      _id: {
        title: 'ID'
      },
      role: {
        title: 'Role'
      },
      email: {
        title: 'Email'
      },
      lastName: {
        title: 'Last Name'
      },

    }
    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
      for (let i = 0; i < data.totalPages; i++) {
        this.totalPages[i] = i + 1;
        this.totalDocs = data.totalDocs;
      }
    })
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit(): Observable<any> {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.service.createUser(this.registerForm.value, {headers: this.accessToken.headers}).subscribe(() => {
        this.showToast('top-right', 'success');
        this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
          this.response = data.docs;
          for (let i = 0; i < data.totalPages; i++) {
            this.totalPages[i] = i + 1;
            this.totalDocs = data.totalDocs;
          }
        })
      })
    }
  }

  showToast(position, status) {
    this.toastrService.show(
      status || 'success',
      `User added successfully`,
      {position, status});
  }

}
