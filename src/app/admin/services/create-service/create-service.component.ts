import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../../../environments/environment";
import {ServicesService} from "../services.service";

@Component({
  selector: 'app-create-service',
  templateUrl: './create-service.component.html',
  styleUrls: ['./create-service.component.scss']
})
export class CreateServiceComponent implements OnInit {
  totalDocs;
  totalPages = [];
  page;
  isEditPage = false;
  columns: {}
  response;
  data;
  file;
  color;
  image_src = {};
  imageUpload;
  id;
  editImage = [];
  object = {
    title: '',
    slug: '',
    miniDescription: '',
    description: '',
    mainProductImage: {}
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: ServicesService) {
    this.file = []

    route.queryParams.subscribe(p => {
      if (p.page) {
        this.page = Number(p.page);
      } else {
        this.page = 1;
        this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
      }

      this.editImage = []
      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById(this.id, this.page).subscribe(data => {
          this.object = data;
          if (data.mainProductImage && data.mainProductImage.path) {
            this.image_src = data.mainProductImage;
            this.editImage = [{
              url: `${environment.api_url}/api/` + data.mainProductImage.path.replaceAll('\\', '/'),
              fileName: data.mainProductImage.description
            }];

          }


        });
      }
    });

  }


  ngOnInit(): void {

    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
      this.totalDocs = data.totalDocs;
      for (let i = 0; i < data.totalPages; i++) {
        this.totalPages[i] = i + 1;
        this.totalDocs = data.totalDocs;
      }
    });
    this.imageUpload = `${environment.api_url}/api/upload/service/image`;

    this.columns = {
      id: {
        title: 'ID'
      },
      title: {
        title: 'TITLE'
      },
      slug: {
        title: 'SLUG'
      },
      miniDescription: {
        title: 'Mini Description'
      }
    }

  }

  nextPage() {
    this.page += 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  getPage(page) {
    this.page = Number(page);
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  prevPage() {
    this.page -= 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get(this.page).subscribe(data => {
      this.response = data.docs;
    })
  }


  open(dialog: TemplateRef<any>) {
    this.editImage = []
    this.image_src = [];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      title: '',
      slug: '',
      miniDescription: '',
      description: '',
      mainProductImage: {}
    };
  }


  onSubmit() {
    this.object.mainProductImage = this.image_src;


    if (this.isEditPage) {
      this.service.updateService(JSON.stringify(this.object), this.id, {headers: this.accessToken.headers}).subscribe(res => {
          this.service.get(this.page).subscribe(data => {
            this.response = data.docs;
            for (let i = 0; i < data.totalPages; i++) {
              this.totalPages[i] = i + 1;
              this.totalDocs = data.totalDocs;
            }
          });
          this.service.getById(this.id, this.page).subscribe(data => {
            // @ts-ignore
            this.editImage = [{
              url: `${environment.api_url}/api/` + data.mainProductImage.path.replaceAll('\\', '/'),
              fileName: data.mainProductImage.description
            }];

          });
        }
      );
    } else {
      this.service.createService(JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.service.get(this.page).subscribe(data => {
          this.response = data.docs;
          this.totalDocs = data.totalDocs;
        });
      });
    }

  }

  uploadFinished(event) {

    if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body) {
      this.image_src = {
        path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
        mimetype: event.serverResponse.response.body.mimetype,
        description: event.serverResponse.response.body.filename
      };

    }
  }

  removeImage() {
    this.image_src = [];
    this.editImage = [];
  }
}
