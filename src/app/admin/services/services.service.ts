import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http: HttpClient) {
  }

  get(page): Observable<any> {
    return this.http.get(environment.api_url + `/api/services?page=${page}`);
  }
  getById(id, page): Observable<any> {
    return this.http.get(environment.api_url + `/api/services/${id}?page=${page}`);
  }

  createService(service, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/services?limit=9999999999999999999', service, options);
  }

  updateService(service, id, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/services/${id}`, service, options);
  }

}
