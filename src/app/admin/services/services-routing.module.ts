import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateServiceComponent} from "./create-service/create-service.component";

const routes: Routes = [
  {
    path: '',
    component: CreateServiceComponent,
    data: {name: 'Services'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule {
}

export const routedComponents = [
  CreateServiceComponent
];
