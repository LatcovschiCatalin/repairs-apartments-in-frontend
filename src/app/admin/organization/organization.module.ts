import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateOrganizationComponent} from './create-organization/create-organization.component';
import {OrganizationService} from "./organization.service";
import {OrganizationRoutingModule} from "./organization-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {ImageUploadModule} from "angular2-image-upload";


@NgModule({
  declarations: [CreateOrganizationComponent],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
    ImageUploadModule.forRoot(),
  ],
  providers: [OrganizationService]
})
export class OrganizationModule {
}
