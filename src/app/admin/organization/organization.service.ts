import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(private http: HttpClient) {
  }

  get(): Observable<any> {
    return this.http.get(environment.api_url + `/api/organization`);
  }

  getById(id): Observable<any> {
    return this.http.get(environment.api_url + `/api/organization/${id}`);
  }

  createOrganization(organization, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/organization?limit=9999999999999999999', organization, options);
  }

  updateOrganization(organization, id, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/organization/${id}`, organization, options);
  }
}
