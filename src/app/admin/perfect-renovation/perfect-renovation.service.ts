import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PerfectRenovationService {

  constructor(private http: HttpClient) {
  }

  get(): Observable<any> {
    return this.http.get(environment.api_url + `/api/perfect-renovation`);
  }

  getById(id): Observable<any> {
    return this.http.get(environment.api_url + `/api/perfect-renovation/${id}`);
  }

  createPerfectRenovation(perfect_renovation, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/perfect-renovation?limit=9999999999999999999', perfect_renovation, options);
  }

  updatePerfectRenovation(perfect_renovation, id, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/perfect-renovation/${id}`, perfect_renovation, options);
  }
}
