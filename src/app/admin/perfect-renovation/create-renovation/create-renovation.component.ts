import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CompanyAdvantagesService} from "../../company-advantages/company-advantages.service";
import {environment} from "../../../../environments/environment";
import {PerfectRenovationService} from "../perfect-renovation.service";

@Component({
  selector: 'app-create-renovation',
  templateUrl: './create-renovation.component.html',
  styleUrls: ['./create-renovation.component.scss']
})
export class CreateRenovationComponent implements OnInit {
  add_renovation = false;
  isEditPage = false;
  columns: {}
  response;
  data;
  id;
  renovation = [
    {
      title: '',
      description: ''
    }
  ];
  object = {
    title: '',
    renovation: [],
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: PerfectRenovationService) {

    route.queryParams.subscribe(p => {

      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById(this.id).subscribe(data => {
          this.object = data;
          this.renovation = data.renovation;


        });
      }
    });

  }


  ngOnInit(): void {

    this.service.get().subscribe(data => {
      this.response = data.docs;
      if (data.docs.length === 0) this.add_renovation = true;
    });

    this.columns = {
      id: {
        title: 'ID'
      },
      title: {
        title: 'TITLE'
      },
    }

  }

  addRenovation() {
    this.renovation.push({
      title: '',
      description: ''
    })
  }

  deleteRenovation(index) {
    this.renovation.splice(index, 1);
  }

  open(dialog: TemplateRef<any>) {
    this.renovation = [
      {
        title: '',
        description: ''
      }
    ];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      title: '',
      renovation: this.renovation,
    };
  }


  onSubmit() {


    if (this.isEditPage) {
      this.service.updatePerfectRenovation(JSON.stringify(this.object), this.id, {headers: this.accessToken.headers}).subscribe(res => {
          this.service.get().subscribe(data => {
            this.response = data.docs;
          });
        }
      );
    } else {
      this.add_renovation = false;
      this.service.createPerfectRenovation(JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.service.get().subscribe(data => {
          this.response = data.docs;
        });
      });
    }
  }


}
