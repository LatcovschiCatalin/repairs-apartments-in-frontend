import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) {
  }

  get(page): Observable<any> {
    return this.http.get(environment.api_url + `/api/blog?page=${page}`);
  }

  getById(id, page): Observable<any> {
    return this.http.get(environment.api_url + `/api/blog/${id}?page=${page}`);
  }

  createBlog(blog, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/blog?limit=9999999999999999999', blog, options);
  }

  updateBlog(blog, id, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/blog/${id}`, blog, options);
  }
}
