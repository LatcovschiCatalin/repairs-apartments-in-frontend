import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class GoalsService {


  constructor(private http: HttpClient) {
  }

  get(): Observable<any> {
    return this.http.get(environment.api_url + `/api/goals`);
  }

  getById(id): Observable<any> {
    return this.http.get(environment.api_url + `/api/goals/${id}`);
  }

  createGoal(goal, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/goals?limit=9999999999999999999', goal, options);
  }

  updateGoal(goal, id, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/goals/${id}`, goal, options);
  }
}
