import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {GoalsService} from "../goals.service";

@Component({
  selector: 'app-create-goal',
  templateUrl: './create-goal.component.html',
  styleUrls: ['./create-goal.component.scss']
})
export class CreateGoalComponent implements OnInit {

  add_goals = false;
  isEditPage = false;
  columns: {}
  response;
  data;
  id;
  goals = [
    {
      title: '',
      description: ''
    }
  ];
  object = {
    title: '',
    goals: [],
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: GoalsService) {

    route.queryParams.subscribe(p => {

      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById(this.id).subscribe(data => {
          this.object = data;
          this.goals = data.goals;


        });
      }
    });

  }


  ngOnInit(): void {

    this.service.get().subscribe(data => {
      this.response = data.docs;
      if (data.docs.length === 0) this.add_goals = true;
    });

    this.columns = {
      id: {
        title: 'ID'
      },
      title: {
        title: 'TITLE'
      },
    }

  }

  addGoal() {
    this.goals.push({
      title: '',
      description: ''
    })
  }

  deleteGoal(index) {
    this.goals.splice(index, 1);
  }

  open(dialog: TemplateRef<any>) {
    this.goals = [
      {
        title: '',
        description: ''
      }
    ];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      title: '',
      goals: this.goals,
    };
  }


  onSubmit() {


    if (this.isEditPage) {
      this.service.updateGoal(JSON.stringify(this.object), this.id, {headers: this.accessToken.headers}).subscribe(res => {
          this.service.get().subscribe(data => {
            this.response = data.docs;
          });
        }
      );
    } else {
      this.add_goals = false;
      this.service.createGoal(JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.service.get().subscribe(data => {
          this.response = data.docs;
        });
      });
    }
  }

}
