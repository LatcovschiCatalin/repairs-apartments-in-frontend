import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateGoalComponent} from "./create-goal/create-goal.component";

const routes: Routes = [
  {
    path: '',
    component: CreateGoalComponent,
    data: {name: 'Goals'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoalsRoutingModule {
}

export const routedComponents = [
  CreateGoalComponent
];
