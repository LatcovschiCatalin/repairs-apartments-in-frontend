import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from "./auth/auth-guard.service";
import {NotFoundComponent} from './@theme/components/miscellaneous/not-found/not-found.component';





export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./landing/landing.module').then(m => m.LandingModule)
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    data: {
      expectedRole: 'admin'
    },
    loadChildren: () => import('./admin/admin.module')
      .then(m => m.AdminModule),
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
