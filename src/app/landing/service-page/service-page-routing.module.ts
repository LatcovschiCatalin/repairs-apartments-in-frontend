import {RouterModule, Routes} from '@angular/router';
import {NgModule} from "@angular/core";
import {
  ResolveService1,
  ResolveService7
} from "../../resolve.service";
import {ServicePageComponent} from "./service-page.component";

const routes: Routes = [
  {
    path: '',
    component: ServicePageComponent,
    data: {name: 'ServicePage'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver7: ResolveService7,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicePageRoutingModule {
}

export const routedComponents = [
  ServicePageComponent
];

