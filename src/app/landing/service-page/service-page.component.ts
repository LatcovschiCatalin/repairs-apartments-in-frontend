import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-service-page',
  templateUrl: './service-page.component.html',
  styleUrls: ['./service-page.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class ServicePageComponent implements OnInit {

  service_param;
  service;
  services;
  company;
  return = true;

  constructor(private route: Router, private routes: ActivatedRoute, private router: Router) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0]
      this.services = response.routeResolver7.docs;
    });

    this.routes.params.subscribe(params => {
      this.service_param = params['id'];
      for (let i = 0; i < this.services.length; i++) {
        if (this.services[i].slug === this.service_param) {
          this.service = this.services[i];
          for (let i = 0; i < this.services.length; i++) {

            if (this.services[i]._id === this.service._id) {
              this.return = false;
              break;
            }

          }

        }
        if (this.services[i]._id === this.service_param) {
          this.service = this.services[i];
          for (let i = 0; i < this.services.length; i++) {

            if (this.services[i]._id === this.service._id) {
              this.return = false;
              break;
            }

          }

        }
      }
    });
  }

  ngOnInit(): void {
    if (this.return) {
      this.router.navigate(['services'])
    }
  }


}
