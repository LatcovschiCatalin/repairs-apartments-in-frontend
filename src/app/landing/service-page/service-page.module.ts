import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicePageComponent } from './service-page.component';
import {ServicePageRoutingModule} from "./service-page-routing.module";
import {SharedModule} from "../shared/shared.module";
import {PipesModule} from "../../pipes/pipes.module";



@NgModule({
  declarations: [ServicePageComponent],
  imports: [
    CommonModule,
    ServicePageRoutingModule,
    SharedModule,
    PipesModule
  ]
})
export class ServicePageModule { }
