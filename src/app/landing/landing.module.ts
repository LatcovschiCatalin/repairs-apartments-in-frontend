import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandingComponent} from './landing.component';
import {HomeModule} from "./home/home.module";
import {LandingRoutingModule} from "./landing-routing.module";
import {PipesModule} from "../pipes/pipes.module";
import {ServiceModule} from "./services/service.module";
import {ContactsModule} from "./contacts/contacts.module";
import {PhotoAlbumsModule} from "./photo-albums/photo-albums.module";
import {AboutUsModule} from "./about-us/about-us.module";


@NgModule({
  declarations: [LandingComponent],
  imports: [
    CommonModule,
    HomeModule,
    LandingRoutingModule,
    PipesModule,
    ServiceModule,
    ContactsModule,
    PhotoAlbumsModule,
    AboutUsModule
  ]
})
export class LandingModule {
}
