import {RouterModule, Routes} from '@angular/router';
import {NgModule} from "@angular/core";
import {
  ResolveService1,
  ResolveService9
} from "../../resolve.service";
import {AboutUsComponent} from "./about-us.component";

const routes: Routes = [
  {
    path: '',
    component: AboutUsComponent,
    data: {name: 'AboutUs'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver9: ResolveService9,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsRoutingModule {
}

export const routedComponents = [
  AboutUsComponent
];

