import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  company;

  constructor(private routes: ActivatedRoute) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0]

    })
  }

  ngOnInit(): void {


  }

}
