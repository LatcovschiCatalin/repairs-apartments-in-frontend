import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home.component";
import {
  ResolveService1,
  ResolveService2,
  ResolveService3,
  ResolveService4,
  ResolveService5, ResolveService6, ResolveService7, ResolveService8
} from "../../resolve.service";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {name: 'Home'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver2: ResolveService2,
      routeResolver3: ResolveService3,
      routeResolver4: ResolveService4,
      routeResolver5: ResolveService5,
      routeResolver6: ResolveService6,
      routeResolver7: ResolveService7,
      routeResolver8: ResolveService8,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}

export const routedComponents = [
  HomeComponent
];
