import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {HomeService} from "./home.service";
import {environment} from "../../../environments/environment";
import {HOME_ITEMS} from "./home-items";
import {ActivatedRoute, Router} from "@angular/router";
import {NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF} from '@web-aid-kit/ngx-image-gallery';
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],

})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  // gallery configuration
  conf: GALLERY_CONF = {
    imageOffset: '30px',
    showDeleteControl: false,
    showImageTitle: true,
  };

  // gallery images
  albumsImages: GALLERY_IMAGE[] = [];
  servicesImages: GALLERY_IMAGE[] = [];
  slideConfig = {
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: false,
    arrows: true,
    dots: false,
    infinite: true,
    speed: 1000,
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          autoplay: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true
        }
      },
      {
        breakpoint: 601,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          arrows: false
        }
      }
    ]

  };


  breakpoint(e) {
  }

  company;
  services = [];
  perfect_renovations;
  goals;
  organizations;
  blogs;
  albums = [];
  services_gallery: boolean;
  imageUrl = environment.api_url + '/api/';
  home_items = HOME_ITEMS;
  id;
  index;
  max_index;
  services_max_length = 0;


  constructor(private service: HomeService, private routes: ActivatedRoute, private router: Router, private cookieService: CookieService) {
    this.routes.data.subscribe((response: any) => {

      this.company = response.routeResolver1.docs[0]
      for (let i = 0; i < response.routeResolver2.docs.length; i++) {
        for (let j = 0; j < response.routeResolver2.docs[i].albums.length; j++) {
          if (this.albums.length === 10) break;
          this.albums.push(response.routeResolver2.docs[i].albums[j]);

        }
      }
      this.perfect_renovations = response.routeResolver3.docs[0]
      this.goals = response.routeResolver4.docs[0]
      this.organizations = response.routeResolver5.docs[0]
      this.blogs = response.routeResolver6.docs

      if (response.routeResolver7.docs.length < 10) {
        this.services_max_length = response.routeResolver7.docs.length;
      } else {
        this.services_max_length = 10;
      }

      for (let i = 0; i < Number(this.services_max_length); i++) {
        this.services[i] = response.routeResolver7.docs[i];
      }
    })


    routes.queryParams.subscribe(p => {

      if (p.ServiceGalleryId) {
        this.services_gallery = true;
        this.index = Number(p.ServiceGalleryId) - 1;
        this.max_index = Number(this.services.length) - 1;
      } else if (p.PhotoAlbumsGalleryId) {
        this.services_gallery = false;
        this.index = Number(p.PhotoAlbumsGalleryId) - 1;
        this.max_index = Number(this.albums.length) - 1;


      }

    })

  }


  ngAfterViewInit() {


    if (this.index >= 0 && this.index <= this.max_index) {
      this.openGallery(this.index)
    } else {
      this.router.navigate(['.'], {relativeTo: this.routes});
    }

  }

  ngOnInit(): void {


    for (let i = 0; i < this.services.length; i++) {
      this.servicesImages.push({
        url: this.imageUrl + this.services[i].mainProductImage.path,
        extUrl: this.imageUrl + this.services[i].mainProductImage.path,
        altText: 'Здесь нет картинок',
        title: this.services[i].title,
        thumbnailUrl: this.imageUrl + this.services[i].mainProductImage.path

      })
    }
    for (let j = 0; j < this.albums.length; j++) {
      this.albumsImages.push({
        url: this.imageUrl + this.albums[j].mainProductImage.path,
        extUrl: this.imageUrl + this.albums[j].mainProductImage.path,
        altText: 'Здесь нет картинок',
        title: this.albums[j].title,
        thumbnailUrl: this.imageUrl + this.albums[j].mainProductImage.path

      })
    }

  }


  openServiceGallery(index) {
    this.services_gallery = true;
    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {ServiceGalleryId: index + 1}});


  }

  openAlbumsGallery(index) {
    this.services_gallery = false;
    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {PhotoAlbumsGalleryId: index + 1}});

  }

  openGallery(index: number = 0) {

    setTimeout(() => {
      this.ngxImageGallery.open(index);

    }, 1)

  }

  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }

  // next image in gallery
  nextImage(index: number = 0) {
    this.ngxImageGallery.next();
  }

  // prev image in gallery
  prevImage(index: number = 0) {
    this.ngxImageGallery.prev();
  }

  /**************************************************/

  // EVENTS
  // callback on gallery opened


  // callback on gallery closed
  galleryClosed() {
    this.router.navigate(['.'], {relativeTo: this.routes});
  }

  // callback on gallery image clicked
  galleryImageClicked(index) {

  }

  galleryOpened(index) {
    setTimeout(() => {
      if (this.services_gallery) {
        this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {ServiceGalleryId: index + 1}});
      } else {
        this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {PhotoAlbumsGalleryId: index + 1}});
      }

    }, 2)
  }


  // callback on gallery image changed
  galleryImageChanged(index) {


    if (this.services_gallery) {
      this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {ServiceGalleryId: index + 1}});
    } else {
      this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {PhotoAlbumsGalleryId: index + 1}});
    }
  }

}
