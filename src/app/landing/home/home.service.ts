import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) {
  }

  getCompany(): Observable<any> {
    return this.http.get(environment.api_url + '/api/company?limit=99999999999999');
  }

  getServices(): Observable<any> {
    return this.http.get(environment.api_url + '/api/services?limit=99999999999999');
  }

  getRenovations(): Observable<any> {
    return this.http.get(environment.api_url + '/api/perfect-renovation?limit=99999999999999');
  }

  getGoals(): Observable<any> {
    return this.http.get(environment.api_url + '/api/goals?limit=99999999999999');
  }

  getOrganization(): Observable<any> {
    return this.http.get(environment.api_url + '/api/organization?limit=99999999999999');
  }

  getAlbums(): Observable<any> {
    return this.http.get(environment.api_url + '/api/photo-albums?limit=99999999999999');
  }

  getBlogs(): Observable<any> {
    return this.http.get(environment.api_url + '/api/blog?limit=99999999999999');
  }

}
