import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServicesComponent} from './services.component';
import {SharedModule} from "../shared/shared.module";
import {ServiceRoutingModule} from "./service-routing.module";
import {ServicePageModule} from "../service-page/service-page.module";
import {NgxImageGalleryModule} from "@web-aid-kit/ngx-image-gallery";
import {AlbumPopupModule} from "../shared/album-popup/album-popup.module";


@NgModule({
  declarations: [ServicesComponent],
  imports: [
    CommonModule,
    SharedModule,
    ServiceRoutingModule,
    ServicePageModule,
    NgxImageGalleryModule,
    AlbumPopupModule

  ],
})
export class ServiceModule {
}
