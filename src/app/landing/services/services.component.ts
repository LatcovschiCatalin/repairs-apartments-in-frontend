import {AfterViewInit, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {SERVICES_ITEMS} from "./services.items";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {GALLERY_CONF, GALLERY_IMAGE, NgxImageGalleryComponent} from "@web-aid-kit/ngx-image-gallery";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit, AfterViewInit {
  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;

  // gallery configuration
  conf: GALLERY_CONF = {
    imageOffset: '30px',
    showDeleteControl: false,
    showImageTitle: true,
  };

  // gallery images
  images: GALLERY_IMAGE[] = [];
  services_items = SERVICES_ITEMS;
  company;
  services;
  imageUrl = environment.api_url + '/api/';
  service_page_path = '';
  hover = false;
  hover_image = false;
  service;
  index;
  max_index;
  width = window.innerWidth;

  constructor(private routes: ActivatedRoute, private router: Router) {
    this.routes.data.subscribe((response: any) => {

      this.company = response.routeResolver1.docs[0];
      this.services = response.routeResolver7.docs;

      for (let i = 0; i < this.services.length; i++) {
        this.images.push({
          url: this.imageUrl + this.services[i].mainProductImage.path,
          extUrl: this.imageUrl + this.services[i].mainProductImage.path,
          altText: 'Здесь нет картинок',
          title: this.services[i].title,
          thumbnailUrl: this.imageUrl + this.services[i].mainProductImage.path

        })
      }


    })


    routes.queryParams.subscribe(p => {

      if (p.ServiceGalleryId) {
        this.index = Number(p.ServiceGalleryId) - 1;
        this.max_index = Number(this.services.length) - 1;
      }

    })

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth;
  }
  ngAfterViewInit() {


    if (this.index >= 0 && this.index <= this.max_index) {
      this.openGallery(this.index)
    } else {
      this.router.navigate(['.'], {relativeTo: this.routes});
    }

  }

  ngOnInit(): void {


  }


  getPath(path) {
    if (!this.hover) {
      this.router.navigate(['/services', path]);
    }
    this.service_page_path = path;
  }



  openGallery(index: number = 0) {

    setTimeout(() => {
      this.ngxImageGallery.open(index);

    }, 1)

  }

  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }

  // next image in gallery
  nextImage(index: number = 0) {
    this.ngxImageGallery.next();
  }

  // prev image in gallery
  prevImage(index: number = 0) {
    this.ngxImageGallery.prev();
  }

  /**************************************************/

  // EVENTS
  // callback on gallery opened


  // callback on gallery closed
  galleryClosed() {
    this.router.navigate(['.'], {relativeTo: this.routes});
  }

  // callback on gallery image clicked
  galleryImageClicked(index) {

  }

  galleryOpened(index) {
    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {ServiceGalleryId: index + 1}});

  }


  // callback on gallery image changed
  galleryImageChanged(index) {

    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {ServiceGalleryId: index + 1}});

  }


}
