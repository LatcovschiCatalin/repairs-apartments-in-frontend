import {RouterModule, Routes} from '@angular/router';
import {ServicesComponent} from "./services.component";
import {NgModule} from "@angular/core";
import {
  ResolveService1,
  ResolveService7
} from "../../resolve.service";

const routes: Routes = [
  {
    path: '',
    component: ServicesComponent,
    data: {name: 'Services'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver7: ResolveService7,
    },
  },
  {
    path: 'services/:id',
    loadChildren: () => import('../service-page/service-page.module').then(m => m.ServicePageModule),

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule {
}

export const routedComponents = [
  ServicesComponent
];

