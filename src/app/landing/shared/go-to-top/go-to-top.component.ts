import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-go-to-top',
  templateUrl: './go-to-top.component.html',
  styleUrls: ['./go-to-top.component.scss']
})
export class GoToTopComponent implements OnInit {

  isTop;
  company;

  constructor(private routes: ActivatedRoute) {
    this.routes.data.subscribe((response: any) => {

      this.company = response.routeResolver1.docs[0];

    })
  }
  getTopPage() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    this.isTop = Number(scrollPosition) >= window.innerHeight;
  }

  ngOnInit() {


  }

}
