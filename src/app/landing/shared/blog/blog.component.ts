import {AfterViewInit, Component, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {GALLERY_CONF, GALLERY_IMAGE, NgxImageGalleryComponent} from "@web-aid-kit/ngx-image-gallery";

@Component({
  selector: 'app-blogs',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit, AfterViewInit {

  @Input() home;
  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  // gallery configuration
  conf: GALLERY_CONF = {
    imageOffset: '30px',
    showDeleteControl: false,
    showImageTitle: true,
  };

  // gallery images
  images: GALLERY_IMAGE[] = [];
  company;
  blogs = [];
  imageUrl = environment.api_url + '/api/';
  hover = false;
  hover_image = false;
  blog;
  index;
  max_index;
  width = window.innerWidth;
  max_length;


  constructor(private routes: ActivatedRoute, private router: Router) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0];
      if (this.home) {
        this.max_length = 10;
      } else {
        this.max_length = response.routeResolver6.docs.length;
      }

      for (let i = 0; i < this.max_length; i++) {
        this.images.push({
          url: this.imageUrl + response.routeResolver6.docs[i].mainProductImage.path,
          extUrl: this.imageUrl + response.routeResolver6.docs[i].mainProductImage.path,
          altText: ' Здесь нет картинок',
          title: response.routeResolver6.docs[i].title,
          thumbnailUrl: this.imageUrl + response.routeResolver6.docs[i].mainProductImage.path

        })
        this.blogs.push(response.routeResolver6.docs[i])
      }

    })


    routes.queryParams.subscribe(p => {

      if (p.BlogGalleryId) {
        this.index = Number(p.BlogGalleryId) - 1;
        this.max_index = Number(this.blogs.length) - 1;
      }

    })

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth;
  }

  ngAfterViewInit() {


    if (this.index >= 0 && this.index <= this.max_index) {
      this.openGallery(this.index)
    } else {
      this.router.navigate(['.'], {relativeTo: this.routes});
    }

  }

  ngOnInit(): void {


  }

  getPath(path) {
    if (!this.hover) {
      this.router.navigate(['/blog', path]);
    }
  }


  openGallery(index: number = 0) {

    setTimeout(() => {
      this.ngxImageGallery.open(index);

    }, 1)


  }

  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();
  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }

  // next image in gallery
  nextImage(index: number = 0) {
    this.ngxImageGallery.next();
  }

  // prev image in gallery
  prevImage(index: number = 0) {
    this.ngxImageGallery.prev();
  }

  /**************************************************/

  // EVENTS
  // callback on gallery opened


  // callback on gallery closed
  galleryClosed() {
    this.router.navigate(['.'], {relativeTo: this.routes});
  }

  // callback on gallery image clicked
  galleryImageClicked(index) {

  }

  galleryOpened(index) {
    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {BlogGalleryId: index + 1}});

  }


  // callback on gallery image changed
  galleryImageChanged(index) {

    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {BlogGalleryId: index + 1}});

  }

}
