import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http: HttpClient) {
  }

  getCompany(): Observable<any> {
    return this.http.get(environment.api_url + '/api/company?limit=99999999999999');
  }

  getAdvantages(): Observable<any> {
    return this.http.get(environment.api_url + '/api/company-advantages?limit=99999999999999');
  }

  getBlogs(): Observable<any> {
    return this.http.get(environment.api_url + '/api/blog?limit=99999999999999');
  }

  getAboutUs(): Observable<any> {
    return this.http.get(environment.api_url + '/api/about-us?limit=99999999999999');
  }

  postMessages(object): Observable<any> {
    return this.http.post(environment.api_url + '/api/messages?limit=99999999999999', object);
  }
  postMessageOnEmail(object): Observable<any> {
    return this.http.post(environment.api_url + '/api/messages/email?limit=99999999999999', object);
  }
}
