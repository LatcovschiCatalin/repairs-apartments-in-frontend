
export const CONTACTS_ITEMS = {
  title: "КОНТАКТЫ",
  contact_us: "Свяжитесь с нами",
  name: "Имя",
  phone: "Номер телефона",
  mail: "Электронная почта",
  message: "Сообщение",
  send: "Отправить",
  required: "обязательно",
  toast_message: "Сообщение было успешно отправлено",
  phone_required: "Требуется номер телефона",
  message_required: "требуется сообщение",
  both_required: "Требуется номер телефона и сообщение",
  map_location: "Вы можете найти нас здесь"

}
