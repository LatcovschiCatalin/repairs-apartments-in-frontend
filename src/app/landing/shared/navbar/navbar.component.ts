import {Component, Input, OnInit, HostListener} from '@angular/core';
import {SharedService} from "../shared.service";
import {NAVBAR_ITEMS} from "./navbar-menu";
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() path;
  @Input() home;
  @Input() height;


  company;
  menu = NAVBAR_ITEMS;
  down_navbar;
  scrollHeight;
  click_hamburger = false;
  width = window.innerWidth;
  scroll_top = document.documentElement.scrollTop;

  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.scrollHeight = Number(scrollPosition);
    this.down_navbar = Number(scrollPosition) >= this.height;
    this.scroll_top = document.documentElement.scrollTop;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth;
  }

  constructor(private service: SharedService, private routes: ActivatedRoute) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0]
    })
  }


  ngOnInit(): void {

    const scrollPosition = window.pageYOffset;
    this.down_navbar = Number(scrollPosition) >= this.height;
  }
}
