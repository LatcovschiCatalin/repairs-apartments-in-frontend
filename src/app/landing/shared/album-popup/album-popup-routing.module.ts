import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AlbumPopupComponent} from "./album-popup.component";
import {
  ResolveService1,
  ResolveService2
} from "../../../resolve.service";

const routes: Routes = [
  {
    path: '',
    component: AlbumPopupComponent,
    data: {name: 'AlbumPopup'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver2: ResolveService2
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumPopupRoutingModule {
}

export const routedComponents = [
  AlbumPopupComponent
];
