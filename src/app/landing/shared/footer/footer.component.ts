import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FOOTER_ITEMS} from "./footer.items";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  company;
  footer_items = FOOTER_ITEMS;
  coordinates;

  constructor(private routes: ActivatedRoute) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0];
      this.coordinates = response.routeResolver1.docs[0].coordinates.split(';');
    })
  }

  ngOnInit(): void {

  }

}
