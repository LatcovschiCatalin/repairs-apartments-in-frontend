import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PhotoAlbumsComponent} from './photo-albums.component';
import {PhotoAlbumsRoutingModule} from "./photo-albums-routing.module";
import {SharedModule} from "../shared/shared.module";
import {PipesModule} from "../../pipes/pipes.module";
import {AlbumPopupModule} from "../shared/album-popup/album-popup.module";
import {
  NbAlertModule, NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDialogModule, NbDialogService, NbIconModule, NbInputModule, NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbSelectModule,
  NbSidebarModule, NbThemeModule, NbToastrModule
} from "@nebular/theme";
import {NbEvaIconsModule} from "@nebular/eva-icons";


@NgModule({
  declarations: [PhotoAlbumsComponent],
  imports: [
    CommonModule,
    PhotoAlbumsRoutingModule,
    SharedModule,
    PipesModule,
    AlbumPopupModule,
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbListModule,
    NbCheckboxModule,
    NbSelectModule,
    NbSelectModule,
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    NbEvaIconsModule,
    NbCardModule,
    NbThemeModule.forRoot({name: 'dark'}),
    NbLayoutModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbIconModule,
  ],
  providers: [NbDialogService]
})
export class PhotoAlbumsModule {
}
