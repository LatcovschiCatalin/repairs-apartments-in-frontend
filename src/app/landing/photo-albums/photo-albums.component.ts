import {Component, OnInit} from '@angular/core';
import {PHOTO_ALBUMS_ITEMS} from "./photo-albums.items";
import {ActivatedRoute} from "@angular/router";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-photo-albums',
  templateUrl: './photo-albums.component.html',
  styleUrls: ['./photo-albums.component.scss']
})
export class PhotoAlbumsComponent implements OnInit {

  photo_albums_items = PHOTO_ALBUMS_ITEMS;
  company;
  photo_albums;
  imageUrl = environment.api_url + '/api/';
  hover = false;
  hover_image = false;
  album;

  constructor(private routes: ActivatedRoute) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0]
      this.photo_albums = response.routeResolver2.docs

    })
  }

  ngOnInit(): void {
  }

}
