import {RouterModule, Routes} from '@angular/router';
import {NgModule} from "@angular/core";
import {
  ResolveService1,
  ResolveService2
} from "../../resolve.service";
import {PhotoAlbumsComponent} from "./photo-albums.component";

const routes: Routes = [
  {
    path: '',
    component: PhotoAlbumsComponent,
    data: {name: 'PhotoAlbums'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver2: ResolveService2
    },

    children: [
      {
        path: 'photo-albums/:id',
        loadChildren: () => import('../shared/album-popup/album-popup.module').then(m => m.AlbumPopupModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoAlbumsRoutingModule {
}

export const routedComponents = [
  PhotoAlbumsComponent
];

