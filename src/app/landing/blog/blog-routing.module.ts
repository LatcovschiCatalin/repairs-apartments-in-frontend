import {RouterModule, Routes} from '@angular/router';
import {NgModule} from "@angular/core";
import {
  ResolveService1,
  ResolveService6
} from "../../resolve.service";
import {BlogComponent} from "./blog.component";

const routes: Routes = [
  {
    path: '',
    component: BlogComponent,
    data: {name: 'Blog'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver6: ResolveService6
    }
  },
  {
    path: ':id',
    loadChildren: () => import('../blog-page/blog-page.module').then(m => m.BlogPageModule),

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule {
}

export const routedComponents = [
  BlogComponent
];

