import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BlogComponent} from './blog.component';
import {BlogRoutingModule} from "./blog-routing.module";
import {SharedModule} from "../shared/shared.module";
import {NgxImageGalleryModule} from "@web-aid-kit/ngx-image-gallery";
import {BlogPageModule} from "../blog-page/blog-page.module";


@NgModule({
  declarations: [BlogComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    NgxImageGalleryModule,
    BlogPageModule
  ]
})
export class BlogModule {
}
