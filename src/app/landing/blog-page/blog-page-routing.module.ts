import {RouterModule, Routes} from '@angular/router';
import {NgModule} from "@angular/core";
import {
  ResolveService1,
  ResolveService6
} from "../../resolve.service";
import {BlogPageComponent} from "./blog-page.component";

const routes: Routes = [
  {
    path: '',
    component: BlogPageComponent,
    data: {name: 'BlogPage'},
    resolve: {
      routeResolver1: ResolveService1,
      routeResolver6: ResolveService6,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogPageRoutingModule {
}

export const routedComponents = [
  BlogPageComponent
];

