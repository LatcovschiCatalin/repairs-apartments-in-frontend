import {Pipe, PipeTransform} from "@angular/core";
import {environment} from "../../environments/environment";

@Pipe({
  name: 'imageUrl'
})

export class Image_urlPipe implements PipeTransform {
  apiStorage = environment.api_url + '/api/';

  transform(value: string): any {
    return this.apiStorage + value;
  }
}
