import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'formatDate'
})

export class Format_datePipe implements PipeTransform {
date;
year;
month;
name_month;
day;

  transform(date: string): any {
    this.date = date.split('-');
    this.year = this.date[0];
    this.month = this.date[1];
    this.day = this.date[2].split('T')[0];
    this.name_month = ["0", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

    return this.day + ' ' + this.name_month[Number(this.month)] + ' ' + this.year;
  }
}
