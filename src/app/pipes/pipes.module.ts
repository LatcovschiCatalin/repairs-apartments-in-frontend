import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Image_urlPipe} from "./image_url.pipe";
import {Format_datePipe} from "./format_date.pipe";
import {Safe_htmlPipe} from "./safe_html.pipe";


@NgModule({
  declarations: [Image_urlPipe, Format_datePipe, Safe_htmlPipe],
  imports: [
    CommonModule,
  ],
  exports: [
    Image_urlPipe,
    Format_datePipe,
    Safe_htmlPipe
  ]
})
export class PipesModule {
}
